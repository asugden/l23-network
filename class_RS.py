# RS.py - a class for layer 2/3 pyramidal cell, to be run at 30 Celsius
# identical to the pyramidal cell of Vierling-Claassen 2010, except that the segmentation
# is forced to be odd (in CellM)

from class_CellM import CellM
from neuron import h as nrn

class RS(CellM):
    def __init__(self, pos):
        self.parameters()
        
        CellM.__init__(self, pos, 'RS')

        self.create_dends(self.dendrite_section_properties, self.biophysics_all['cm']) # this is in CellM()
        self.connect_sections()
        self.biophys()
        self.synapse_create()
    
    # PARAMETERS ------------------------------------------------------------------------
    def parameters(self):
        # name, length, diameter
        self.dendrite_section_properties = [
            ('apical_trunk', 35., 2.5),
            ('apical_1', 180., 2.4),
            ('apical_tuft', 140., 2.),
            ('apical_oblique', 200., 2.3),
            
            ('basal_1', 50., 2.5),
            ('basal_2', 150., 1.6),
            ('basal_3', 150, 1.6),
        ]
        
        # connections by name, doesn't slow things down much
        # (connect me, at position, to her, at position)
        # subsets are now 0-4, 5-9, 10-14 describing each of the dendrites branching from the soma
        self.dendrite_connections = [
            ('apical_trunk', 0, 'soma', 1),
            ('apical_1', 0, 'apical_trunk', 1),
            ('apical_tuft', 0, 'apical_1', 1),
            ('apical_oblique', 0, 'apical_trunk', 1),
            
            ('basal_1', 0, 'soma', 0),
            ('basal_2', 0, 'basal_1', 1),
            ('basal_3', 0, 'basal_1', 1),
        ]
        
        
        # list of receptors to add to all dendritic sections
        self.receptors = ['ampa', 'ampaf', 'ampad', 'nmda', 'gabaa', 'gabab', 'gabaaf', 'gababf', 'gabaad', 'gababd']

        # list channels to insert everywhere
        self.channels = ['pas', 'kv', 'na_mainen', 'ca', 'kca', 'km', 'cad']
        
        # biophysics adjustments to be applied to all compartments, soma, and dendrites
        self.biophysics_all = {
            'g_pas':4e-5,
            'e_pas':-65,
            'Ra':200,
            'cm':2.065,
            'ek':-75,
            'ena':60,
            'gbar_kv':800,
            'gbar_na_mainen':5000, # was 5000, then was 20000
            'gbar_km':10,
        }
        
        self.biophysics_soma = {
            'L':13,
            'diam':15.6,
            'gbar_ca':60,
            'gbar_kca':40,
            'taur_cad':100,
        }
        
        self.biophysics_dendrites = {
            'gbar_ca':30,
            'gbar_kca':20,
        }
