# CellM.py - a cell-wide class, of which FS, LTS, and RS are sub-classes
# branched from s1beta class_cell in 9/2012
# WARNING: synapse classes copied from VC2010 but are not necessarily complete, see below

# Units for e: mV
# Units for gbar: S/cm^2 unless otherwise noted
# Units for cm: uF/cm^2 and value is almost always 0.7-1 (or 2 in the case of 2009 on)

from neuron import h as nrn

class CellM():
    def __init__(self, pos, cell_name='Cell'):
        # set position and name
        self.pos = pos
        self.name = cell_name

        # create soma
        self.soma = nrn.Section(cell=self, name=cell_name+'_soma')

    # Creates dendritic sections
    # dend_props is a list of tuples containing name, length, diameter of each dendrite
    # the length of dend_props defines the number of dendritic compartments
    def create_dends(self, dend_props, cm):
        # preallocate list to store dends
        self.list_dend = []
 
        # create dends and set dend props
        for sec_name, L, diam in dend_props:
            # the following line is the one that makes the Neuron section
            self.list_dend.append(nrn.Section(name=self.name+sec_name))
            
            # the -1 index always changes the last element of the list,
            # i.e. if the list is [1,2,3], then the -1 element of list is 3
            self.list_dend[-1].L = L
            self.list_dend[-1].diam = diam
            self.list_dend[-1].Ra = 200
            self.list_dend[-1].cm = cm

            # set nseg for each dend
            # this code sets multiple compartments when the length is greater than 50 um
            if L > 100:
                self.list_dend[-1].nseg = int(L/50)
                
                # make dend.nseg odd for all sections
                if not self.list_dend[-1].nseg%2:
                    self.list_dend[-1].nseg += 1
                    
     # connect sections of this cell together
    def connect_sections(self):
        # set up a dictionary converting dendritic section names to numbers-- fast
        self.matching_dict = {self.dendrite_section_properties[i][0]:i for i in range(len(self.dendrite_section_properties))}
        
        # connect sections together appropriately
        # connect(parent, parent_end, {child_start=0})
        for connect in self.dendrite_connections:
            dend_ind = self.matching_dict[connect[0]]
            if connect[2] == 'soma':
                self.list_dend[dend_ind].connect(self.soma, connect[3], connect[1])
            else:
                parent_ind = self.matching_dict[connect[2]]
                self.list_dend[dend_ind].connect(self.list_dend[parent_ind], connect[3], connect[1])
                
    # Return the section asked for by name using a dictionary. 
    # This removes the need to deal with soma being independent from the dendrite list
    def section(self, name):
        if name == 'soma':
            return self.soma
        elif name == 'all':
        	return [self.soma] + self.list_dend
        else:
            return self.list_dend[self.matching_dict[name]]
            
    def receptor(self, name, type):
    	if type not in self.synapses: return False
    	
    	if name == 'soma': return self.synapses[type][0]
        elif name == 'all': return self.synapses[type]
        else: return self.synapses[type][self.matching_dict[name] + 1]
    
    
    # Add all of the necessary receptors
    def biophys(self):
        # Soma
        for channel in self.channels:
            self.soma.insert(channel)
        for key in self.biophysics_all:
            setattr(self.soma, key, self.biophysics_all[key])
        for key in self.biophysics_soma:
            setattr(self.soma, key, self.biophysics_soma[key])

        # Dendrites
        # Set the distance off of the soma
        nrn.distance(sec = self.soma)
        
        for sect in self.list_dend:
            for channel in self.channels:
                sect.insert(channel)
            for key in self.biophysics_all:
                setattr(sect, key, self.biophysics_all[key])
            for key in self.biophysics_dendrites:
                setattr(sect, key, self.biophysics_dendrites[key])
                
    # Create all possible synapses, exactly as done in the neuron code 
    def synapse_create(self):
        # synapses is a dict so that it can include all types of synapses referenced by name
        self.synapses = dict()
        # iterate through all types of receptors and add them to each dendritic compartment
        for receptor in self.receptors:
            self.synapses[receptor] = [getattr(self, 'syn_%s_create' % (receptor))(self.soma(0.5))]
            for dendrite in self.list_dend:
                self.synapses[receptor].append(getattr(self, 'syn_%s_create' % (receptor))(dendrite(0.5)))
    
    ## For all synapses, section location 'secloc' is being explicitly supplied 
    ## for clarity, even though they are (right now) always 0.5. Might change in future
    # creates a RECEIVING inhibitory synapse at secloc
    def syn_gabaa_create(self, secloc):
        syn_gabaa = nrn.Exp2Syn(secloc)
        syn_gabaa.e = -80
        syn_gabaa.tau1 = 0.5
        syn_gabaa.tau2 = 5
        return syn_gabaa

    # creates a RECEIVING slow inhibitory synapse at secloc
    # called: self.soma_gabab = syn_gabab_create(self.soma(0.5))
    def syn_gabab_create(self, secloc):
        syn_gabab = nrn.Exp2Syn(secloc)
        syn_gabab.e = -80
        syn_gabab.tau1 = 1
        syn_gabab.tau2 = 20
        return syn_gabab

    # creates a RECEIVING excitatory synapse at secloc
    def syn_ampa_create(self, secloc):
        syn_ampa = nrn.Exp2Syn(secloc)
        syn_ampa.e = 0
        syn_ampa.tau1 = 0.5
        syn_ampa.tau2 = 5
        return syn_ampa
        
    # creates a RECEIVING facilitating excitatory synapse at secloc
    def syn_ampaf_create(self, secloc):
        syn_ampaf = nrn.FDSExp2Syn(secloc)
        syn_ampaf.e = 0
        syn_ampaf.tau1 = 0.5
        syn_ampaf.tau2 = 3
        syn_ampaf.f = .2
        syn_ampaf.tau_F = 200
        syn_ampaf.d1 = 1
        syn_ampaf.tau_D1 = 380
        syn_ampaf.d2 = 1
        syn_ampaf.tau_D2 = 9200
        return syn_ampaf

    # creates a RECEIVING depressing excitatory synapse at secloc
    def syn_ampad_create(self, secloc):
        syn_ampad = nrn.FDSExp2Syn(secloc)
        syn_ampad.e = 0
        syn_ampad.tau1 = 0.5
        syn_ampad.tau2 = 3
        syn_ampad.f=.5
        syn_ampad.tau_F = 94
        syn_ampad.d1 = 0.416
        syn_ampad.tau_D1 = 380
        syn_ampad.d2 = 0.975
        syn_ampad.tau_D2 = 9200
        return syn_ampad

    # creates a RECEIVING nmda synapse at secloc
    # this is a pretty fast NMDA, no?
    def syn_nmda_create(self, secloc):
        syn_nmda = nrn.Exp2Syn(secloc)
        syn_nmda.tau1 = 1
        syn_nmda.tau2 = 20
        return syn_nmda
        
    # creates a receiving facilitating gaba_a synapse at position secloc
    def syn_gabaaf_create(self, secloc):
        s = nrn.FDSExp2Syn(secloc)
        s.tau1 = 0.5
        s.tau2 = 10
        s.e = -80
        s.f = .07
        s.tau_F = 94
        s.d1 = .9
        s.tau_D1 = 380
        s.d2 = 1
        s.tau_D2 = 9200
    
    # creates a receiving depressing gaba_a synapse at position secloc
    def syn_gabaad_create(self, secloc):
        s = nrn.FDSExp2Syn(secloc)
        s.tau1 = 0.5
        s.tau2 = 10
        s.e = -80
        s.f = 0.917
        s.tau_F = 94
        s.d1 = 0.416
        s.tau_D1 = 380
        s.d2 = 0.975
        s.tau_D2 = 9200
        
    # creates a receiving facilitating gaba_b synapse at position secloc
    def syn_gababf_create(self, secloc):
        s = nrn.FDSExp2Syn(secloc)
        s.tau1=1
        s.tau2=20
        s.e=-80
        s.f = 0.917
        s.tau_F = 94
        s.d1 = 0.416
        s.tau_D1 = 380
        s.d2 = 0.975
        s.tau_D2 = 9200
    
    # creates a receiving depressing gaba_b synapse at position secloc WARNING, this is the same as gababf
    def syn_gababd_create(self, secloc):
        s = nrn.FDSExp2Syn(secloc)
        s.tau1 = 1
        s.tau2 = 20
        s.e = -80
        s.f = 0.917
        s.tau_F = 94
        s.d1 = 0.416
        s.tau_D1 = 380
        s.d2 = 0.975
        s.tau_D2 = 9200
    
    

    # connect_to_target created for pc, used in Network()
    # these are SOURCES of spikes
    def connect_to_target(self, target):
        nc = nrn.NetCon(self.soma(0.5)._ref_v, target, sec=self.soma)
        nc.threshold = 0

        return nc
    