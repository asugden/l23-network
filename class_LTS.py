# LTS.py - a class for an LTS or SOM cell, to be run at 30 Celsius
# almost identical to the LTS cell of Vierling-Claassen 2010 except that gbar_ar is set 
# per segment of a dendritic compartment instead of the entirety of the dendrite.

from class_CellM import CellM
from neuron import h as nrn
import numpy as np

class LTS(CellM):
    def __init__(self, pos):
        self.parameters()
        
        CellM.__init__(self, pos, 'LTS')

        self.create_dends(self.dendrite_section_properties, self.biophysics_all['cm']) # this is in CellM()
        self.connect_sections()
        self.biophys()
        self.synapse_create()
    
    # PARAMETERS ------------------------------------------------------------------------
    def parameters(self):
        # name, length, diameter
        self.dendrite_section_properties = [
            ('a', 3.4, 3),
            ('b', 8.4, 2),
            ('c', 143, 1.25),
            ('d', 117, 1.25),
            ('e', 140, 1.25),
            
            ('f', 3.4, 3),
            ('g', 8.4, 2),
            ('h', 143, 1.25),
            ('i', 117, 1.25),
            ('j', 140, 1.25),
            
            ('k', 3.4, 3),
            ('l', 8.4, 2),
            ('m', 143, 1.25),
            ('n', 117, 1.25),
            ('o', 140, 1.25),
        ]
        
        # connections by name, doesn't slow things down much
        # (connect me, at position, to her, at position)
        # subsets are now 0-4, 5-9, 10-14 describing each of the dendrites branching from the soma
        self.dendrite_connections = [
            ('a', 0, 'soma', 1),
            ('b', 0, 'a', 1),
            ('c', 0, 'b', 1),
            ('d', 0, 'b', 1),
            ('e', 0, 'a', 1),
            
            ('f', 0, 'soma', 1),
            ('g', 0, 'f', 1),
            ('h', 0, 'g', 1),
            ('i', 0, 'g', 1),
            ('j', 0, 'f', 1),
            
            ('k', 0, 'soma', 1),
            ('l', 0, 'k', 1),
            ('m', 0, 'l', 1),
            ('n', 0, 'l', 1),
            ('o', 0, 'k', 1),
        ]
        
        
        # list of receptors to add to all dendritic sections
        self.receptors = ['ampa', 'nmda', 'gabaa', 'gabab', 'ampaf', 'ampad']

        # list channels to insert everywhere
        self.channels = ['pas', 'kv', 'na_mainen', 'ca', 'kca', 'km', 'cad', 'cat', 'ar', 'kv3']
        
        # biophysics adjustments to be applied to all compartments, soma, and dendrites
        self.biophysics_all = {
            'g_pas':7e-5,
            'e_pas':-74,
            'Ra':200,
            'cm':1,
            'taur_cad':200,
            'ek':-60,
            'ena':75,
            'vshift_na_mainen':-18,
            'eca':140,
            'gbar_kv3':0.3,
            'gbar_km':0,
            'gbar_kca':8, # was 5 
        }
        
        self.biophysics_soma = {
            'L':24.3,
            'diam':11,
            'gbar_na_mainen':7000, # was 7000
            'gbar_kv':350,
            'gbar_ca':30,
#            'gbar_kca':5,
#            'gbar_km':0,
            'gbar_cat':2e-4,
            'gbar_ar':1e-6,
        }
        
        self.biophysics_dendrites = {
            'gbar_na_mainen':7000, # was 7000
            'gbar_kv':350,
            'gbar_ca':30,
 #           'gbar_kca':5,
 #           'gbar_km':0,
            'gbar_cat':2e-4,
        }
    # END PARAMETERS --------------------------------------------------------------------

    # Set the ion style (from original Mainen patdemo code), specific to the LTS cell
    def ion_style(self, sect):
        sect.push()
        nrn.ion_style("ca_ion", 0, 1, 0, 0, 0)
        nrn.pop_section()
        
    # Add all of the necessary receptors
    # Modified from CellM's biophys
    def biophys(self):
        # Soma
        for channel in self.channels:
            self.soma.insert(channel)
        for key in self.biophysics_all:
            setattr(self.soma, key, self.biophysics_all[key])
        for key in self.biophysics_soma:
            setattr(self.soma, key, self.biophysics_soma[key])
        self.ion_style(self.soma)

        # Dendrites
        # Set the distance off of the soma
        nrn.distance(sec = self.soma)
        
        for sect in self.list_dend:
            for channel in self.channels:
                sect.insert(channel)
            self.ion_style(sect)
            for key in self.biophysics_all:
                setattr(sect, key, self.biophysics_all[key])
            for key in self.biophysics_dendrites:
                setattr(sect, key, self.biophysics_dendrites[key])
			            
            # Specific to LTS cell. This is set by dendritic segment, not compartment,
            # unlike Dorea's code
            sect.push()
            for seg in sect:
                seg.gbar_ar = 1e-6*np.exp(0.003*nrn.distance(seg.x))
            nrn.pop_section()
