from class_Network import Network
from neuron import h as nrn
from Grapher import Grapher

class GammaRamp():
	def __init__(self):
		nrn.load_file("stdrun.hoc")
		
		self.n = Network()
		print '\tInitializing network...'
		self.n.test_network()
		#self.n.cortical_network()
		print '\t... initialized'
		
	def run(self):
		print '\tSetting optogenetic ramp...'
		self.n.optogenetic_ramp('rs')
		
		mx = int(round(self.n.network['rs']['size'][0]/2.0))
		my = int(round(self.n.network['rs']['size'][1]/2.0))
		
 		print '\tRecording...'
 		self.n.record(self.n.network['rs']['cells'][mx][my].soma(0.5))
 		nrn.tstop = 3000
 		
 		print '\t...Setup completed.\nRUNNING...'
 		nrn.run()
 		print '\t... completed'
 		
 	def plot(self):
 		print '\tGraphing...'
 		gr = Grapher()
 		fig, ax = gr.startgraph({})
		ax.plot(self.n.time[0].to_python(), self.n.records[0].to_python(), color='gray', linewidth=0.5)
		gr.finishgraph(fig, ax, {})
 		
		
if __name__ == '__main__':
	gr = GammaRamp()
	gr.run()
	gr.plot()