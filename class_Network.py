# Network.py creates a network of FS, LTS, and RS cells with their electrical and chemical
# connections. Takes a seed in init so that the network can be fixed. Without a seed, it 
# generates its own.

import random as ran
import numpy as np

from class_FS import FS
from class_LTS import LTS
from class_RS import RS
from neuron import h as nrn

#import matplotlib as mpl
#mpl.use('Agg', warn=False)
#import matplotlib.pyplot as plt
#from matplotlib.path import Path
import math


class Network():
	def __init__(self, seed=False):
		self.layout_seed = self._random_seed(seed)
			
		# All of our cells must be run at 30 degrees C.
		nrn.celsius = 30
		self.gap_junctions = []
		self.stims = []
		self.records = []
		self.time = []
	
	# ================================================================================== #
	# PARAMETERS
	
	# Save all of the parameters of the chemical connections in a dict for later saving
	# Where can be keyword 'any' or a list of appropriate sections
	# Distance is tuple of (m, b) from y=mx+b
	chemical_connection_pars = [
		{'from': 'rs', 'to': 'rs', 'type': 'ampa', 'where':'any', 'distance': (-0.043, 21)},
		{'from': 'rs', 'to': 'fs', 'type': 'ampad', 'where':'any', 'distance': (-0.14, 70)},
		{'from': 'rs', 'to': 'lts', 'type': 'ampaf', 'where':'any', 'distance': (-0.14, 70)},
		
		{'from': 'fs', 'to': 'rs', 'type': 'gabaad', 'where':['soma'], 'distance': (-0.21, 75)},
		{'from': 'fs', 'to': 'fs', 'type': 'gabaa', 'where':'any', 'distance': (-0.18, 63)},
		{'from': 'fs', 'to': 'lts', 'type': 'gabaa', 'where':'any', 'distance': (-0.15, 52)},
		
		{'from': 'lts', 'to': 'rs', 'type': 'gabaa', 'where':['apical_1', 'apical_tuft', 'apical_oblique'], 'distance': (-0.21, 75)},
		{'from': 'lts', 'to': 'fs', 'type': 'gabaa', 'where':'any', 'distance': (-0.15, 52)},
#		{'from': 'lts', 'to': 'lts', 'type': 'gabaa', 'where':'any', 'distance': (0, 0)},
	]
	
	# Set the parameters for gap junction connections
	# As of now, the distance units are wrong. Gap junctions were tuned more than 
	# chemical connections, requiring a more simple way of describing them
	electrical_connection_pars = {
		'fs': [{
			'probability':0.3,
			'strength':2.0,
			'radius':2, # 100 um
			'inner_radius':0.1, # > 0 um
			'sections':['a', 'b', 'f', 'g', 'k', 'l']
		}, {
			'probability':0.25, 
			'strength':1.5, 
			'radius':4, # 200 um
			'inner_radius':2, # 100 um
			'sections':['c', 'h', 'm', 'd', 'i', 'n', 'e', 'j', 'o']
		}],
		'lts': [{
			'probability':0.35, 
			'strength':1.0, 
			'radius':2, # 100 um
			'inner_radius':0.1, # > 0 um
			'sections':['a', 'b', 'f', 'g', 'k', 'l']
		}, {
			'probability':0.3, 
			'strength':0.5, 
			'radius':4, # 200 um
			'inner_radius':2, # 100 um
			'sections':['c', 'h', 'm', 'd', 'i', 'n', 'e', 'j', 'o']
		}]
	}
	
	simulation_pars = {
		'chemical-connections-groups': 10,
		'chemical-connections-min-group-n': 20,
	}
	
	# ================================================================================== #
	# NETWORK GENERATION
	
	# We designed our full-size grid based on the densities of cells of a 100 um thick
	# slice, i.e. roughly a single cell layer.
	# From measurements in dPOR, we get an inhibitory cell density of 12,500 inhibitory
	# cells per cubic mm, which will be the correct order of magnitude for barrel ctx.
	# Each inhibitory cell takes up ~ 80,000 cubic microns or 45-50 microns soma-to-soma.
	# A 50 um thick slice should be roughly a monolayer of cells, like our network.
	# So, a 10 x 10 grid of interneurons is 500 x 500 um. We know a 300 um x 200 um x 
	# 800 um (isolated vPOR) is sufficient to generate gamma oscillations. Therefore, this
	# network should be more than adequate.
	
	def test_network(self):
		self.cortical_network((4, 4, 50), (4, 4, 50), (11, 11, 500./28.))
	
	# Sizes are tuples of (size(x), size(y), distance between neighboring cells)
	def cortical_network(self, FSsize=(10, 10, 50), LTSsize=(10, 10, 50), RSsize=(28, 28, 500.0/28.0)):
		pos, center = self._positions_in_microns({'fs':FSsize, 'lts':LTSsize, 'rs':RSsize})
		
		self.network = {
			'fs': {
				'cells': self._matrix('FS', FSsize),
				'position': pos['fs'],
				'center': center,
				'size': FSsize,
				'gap-junctions': [],
				'post-synapses': [], # Post synaptic targets
			},
			
			'lts': {
				'cells': self._matrix('LTS', LTSsize),
				'position': pos['lts'],
				'center': center,
				'size': LTSsize,
				'gap-junctions': [],
				'post-synapses': [],
			},
			
			'rs': {
				'cells': self._matrix('RS', RSsize),
				'position': pos['rs'],
				'center': center,
				'size': RSsize,
				'post-synapses': [],
			},
		}
		
		self.network['fs']['gap-junctions'] = self.gj_couple_network(self.network['fs']['cells'], self.electrical_connection_pars['fs'])
		self.network['lts']['gap-junctions'] = self.gj_couple_network(self.network['lts']['cells'], self.electrical_connection_pars['lts'])
		
		for chemcon in self.chemical_connection_pars:
			self.chemically_couple_network(chemcon)
		
	# Define an x by y matrix of cells of type
	# Takes type of cell ('FS', 'LTS', or 'RS') and dimensions as tuple
	# Returns matrix indexed by matrix[x][y]
	def _matrix(self, type, (x, y, dist)):
		if x <= 0 or y <= 0: return []
		
		out = []
		for i in range(x):
			out.append([])
			for j in range(y):
				out[i].append(globals()[type]((x, y)))
		return out
		
	# This function returns the position of each cell of each matrix of cells based on the
	# cell-to-cell distance and the midpoint of the largest matrix of cells.
	# Sizes should be dict by type, for saving
	def _positions_in_microns(self, sizes):
		keylist = [key for key in sizes]
		ws, hs = [sizes[l][0]*sizes[l][2] for l in keylist], [sizes[l][1]*sizes[l][2] for l in keylist]
		midxs, midys = [sizes[l][0] for l in keylist], [sizes[l][1] for l in keylist]
		w, h = max(ws), max(hs)
		cx, cy = w/2.0, h/2.0
		
		out = {'width': w, 'height': h}
		for i in range(len(keylist)):
			sz = sizes[keylist[i]]
			out[keylist[i]] = [[((x - midxs[i])*sz[2] + cx, (y - midys[i])*sz[2] + cy) for y in range(sz[1])] for x in range(sz[0])]
		
		return out, (cx, cy)

	# Get a list of all unidirectional connections from network pre to network post
	# Pars is a value from self.chemical_connection_pars
	def _uniconnections(self, pars, pre, post=[]):
		connectoself = True if pars['from'] != pars['to'] else False
		cons = []
		maxd = -pars['distance'][1]/pars['distance'][0]
		
		for prex in range(pre['size'][0]):
			for prey in range(pre['size'][1]):
				for postx in range(post['size'][0]):
					for posty in range(post['size'][1]):
						dx = post['position'][postx][posty][0] - pre['position'][prex][prey][0]
						dy = post['position'][postx][posty][1] - pre['position'][prex][prey][1]
						d = math.sqrt(dx*dx + dy*dy)
						if d < maxd and (connectoself or prex != postx or prey != posty):
							prob = pars['distance'][0]*d + pars['distance'][1]
							cons.append([prob, prex, prey, postx, posty])
		
		cons = np.array(cons)
		np.sort(cons)
		sz = min(self.simulation_pars['chemical-connections-groups'], len(cons)/self.simulation_pars['chemical-connections-min-group-n'])
		chunks = np.array_split(cons, sz)
		
		out = []
		for chunk in chunks:
			keep = int(np.sum(chunk, axis=0)[0])
			np.random.shuffle(chunk)
			for c in chunk[:keep]:
				out.append([int(c[1]), int(c[2]), int(c[3]), int(c[4])])
		
		return out

	# IF pre and post networks are the same, leave post unset
	def chemically_couple_network(self, pars):
		cons = self._uniconnections(pars, self.network[pars['from']], self.network[pars['to']])
		for con in cons:
			pre = self.network[pars['from']]['cells'][con[0]][con[1]]
			post = self.network[pars['to']]['cells'][con[2]][con[3]]
			if isinstance(pars['where'], list):
				where = post.receptor(ran.choice(pars['where']), pars['type'])
			elif pars['where'] == 'any':
				where = ran.choice(post.receptor('all', pars['type']))
			else:
				where = post.receptor(pars['where'], pars['type']) 
			
			nc = pre.connect_to_target(where)
			self.network[pars['from']]['post-synapses'].append([pars['type'], con[2], con[3], nc])
		
	
	# ====================================================================================
	# EXTERNAL TESTING FUNCTIONS
	
	# Return the minimum and maximum coupling strengths for electrical connections (output
	# of gj_couple_network)
	def minmaxcon(self, cs):
		for i in range(len(cs)):
			if i == 0:
				min = cs[i][6]
				max = cs[i][6]
			elif cs[i][6] > max:
				max = cs[i][6]
			elif cs[i][6] < min:
				min = cs[i][6]
		return (min, max, max-min)
	
	# Draw all possible electrical connections to file(s)
	def draw_connections(self, savepath, size=-1):
		possibles = ['FS', 'LTS']
		savepath = savepath if savepath[-1] == '/' else savepath + '/'
		draw = []
		for possible in possibles:
			if hasattr(self, possible):
				draw.append({'type':possible, 'size':[len(getattr(self, possible)), len(getattr(self, possible)[0])], 'connections':getattr(self, possible + '_connections')})
				
		for net in draw:
			# Axes
			fig = plt.figure(figsize=(net.size[0]/2, net.size[1]/2) if size == -1 else size)
			ax = plt.subplot(111)
			ax.set_xlim(0, net['size'][0]+1)
			ax.set_ylim(0, net['size'][1]+1)
			ax.set_xticks(np.arange(net['size'][0])+1, minor=False)
			ax.set_yticks(np.arange(net['size'][1])+1, minor=False)
			ax.set_xticklabels([i+1 for i in range(net['size'][0])], minor=False)
			ax.set_yticklabels([i+1 for i in range(net['size'][1])], minor=False)
			ax.set_title('ELECTRICAL CONNECTIONS IN CELL NETWORK')
			
			# Spots
			l1 = [int(i/net['size'][0]) + 1 for i in range(net['size'][0]*net['size'][1])]
			l2 = [i%net['size'][0] + 1 for i in range(net['size'][0]*net['size'][1])]
			plt.scatter(l2, l1, s=20, facecolors="w", edgecolors="k", lw=0.5)
			
			# Arcs
			minmax = self.minmaxcon(net['connections'])
			for con in net['connections']:
				xA = con[0]+1
				yA = con[1]+1
				xB = con[2]+1
				yB = con[3]+1
				str = float(con[6] - minmax[0])/minmax[2]
				dx = xB - xA
				dy = yB - yA
			
				dist = dx*dx + dy*dy
				angle = math.atan(dy/dx) + 3*math.pi/2 if dx != 0 else 2*math.pi
				jitter = [math.cos(angle)*math.log(math.sqrt(dist)), math.sin(angle)*math.log(math.sqrt(dist))]
			
				verts = [(xA, yA), ((xA+xB)*0.5 + jitter[0], (yA+yB)*0.5 + jitter[1]), (xB, yB)]
				codes = [Path.MOVETO, Path.CURVE3, Path.CURVE3]
				ax.add_patch(mpl.patches.PathPatch(Path(verts, codes), facecolor="none", edgecolor=(str, 0, 1-str), lw=0.5))

			plt.savefig(savepath + 'connections-'+net['type']+'.pdf')		
			plt.close(fig)
			
	# Set the figure font properties
	def font(self, font={'family':'Helvetica', 'weight':'light', 'size':10}):
		mpl.rc('font', **font)
		return self
	
	# Define a stimulus
	# Takes segment and properties dict. ex. {'delay':100, 'dur':300, 'amp':-0.3}
	def stimulus(self, segment, properties):
		stim = nrn.IClamp(segment)
		for key in properties:
			setattr(stim, key, properties[key])
		self.stims.append(stim)
		
	# Takes a segment and properties dur, amp
	# Designed for a single time vector for a single ramp across all injected cells.
	def ramp(self, segment, properties):
		if not hasattr(self, 'ramp_time_vector'):
			self.ramp_time_vector = nrn.Vector()
			self.ramp_time_vector.from_python([0, properties['dur']])
			self.ramp_time_vector_dur = properties['dur']
		elif properties['dur'] != self.ramp_time_vector_dur:
			print 'ERROR: injected ramps not of the same duration'
			return False
		
		stim = nrn.IClamp(segment)
		setattr(stim, 'delay', 0)
		setattr(stim, 'dur', 1e9)
		c = nrn.Vector()
		c.from_python([0, properties['amp']])
		c.play(stim._ref_amp, self.ramp_time_vector)
		
		self.stims.append(c)
	
	def optogenetic_ramp(self, net='rs', photocurrent_mean=6000, photocurrent_stdev=5000):
		for x in range(self.network[net]['size'][0]):
			for y in range(self.network[net]['size'][1]):
				dx = self.network[net]['position'][x][y][0] - self.network[net]['center'][0]
				dy = self.network[net]['position'][x][y][1] - self.network[net]['center'][1]
				d = math.sqrt(dx*dx + dy*dy)
				
				power = max(0, ran.gauss(photocurrent_mean, photocurrent_stdev))
				if d >= 500: power = 0
				elif d > 0: power = power*(1.0 - d/500)
				
				if power > 0:
					self.ramp(self.network[net]['cells'][x][y].section('soma')(0.5), {'dur':3000, 'amp':power})
	
	# Define a recording site.
	def record(self, segment):
		if len(self.time) == 0:
			t = nrn.Vector()
			t.record(nrn._ref_t)
			self.time.append(t)
		
		v = nrn.Vector()
		v.record(segment._ref_v)
		self.records.append(v)
		
	# Define a recording site.
	def vclamp(self, segment):
		if len(self.time) == 0:
			t = nrn.Vector()
			t.record(nrn._ref_t)
			self.time.append(t)
		
#		v = nrn.Vector()
#		v.record(segment._ref_v)
#		self.records.append(v)
		
	# Clear all stimuli
	def clearstimuli(self):
		self.stims = []
		
	# Clear all recording sites
	def clearrecords(self):
		self.records = []
		self.time = []
	
	# ====================================================================================
	# INTERNAL FUNCTIONS
	
	# Set random seed. If seed is unset, set a random seed and return the seed used
	# Takes optional seed as input
	def _random_seed(self, seed=False):
		if seed is False:
			ran.seed()
			np.random.seed()
		else:
			ran.seed(seed)
			np.random.seed(seed + 1)
		return seed
	
	# Electrically couple a network (from self.matrix) and return connections for display
	# Takes a network and list of dictionaries, ex:
	# [{'probability', 'strength', 'radius', 'inner_radius'. 'sections':['a', 'b']}]
	# Returns a list of tuples (xA [pos x of cell A], yA, xB, yB, secA, secB, strength)
	def gj_couple_network(self, net, pars):
		# Return the next unused section. Shuffle the list initially. 
		def getsection(conseclist, x, y):
			if conseclist[x][y][1] == 0:
				ran.shuffle(conseclist[x][y][0])
			out = conseclist[x][y][0][conseclist[x][y][1]%len(conseclist[x][y][0])]
			conseclist[x][y][1] += 1
			return out
			
		w = len(net)
		h = len(net[0])
		allconnections = []
		
		# Generate a list of connections, shuffle it, and trim it to length
		for contype in pars:
			cons = self._biconnections((w, h), contype["radius"], contype["inner_radius"])
			ran.shuffle(cons)
			cons = cons[:int(contype["probability"]*len(cons))]
			consec = [[[list(contype["sections"]), 0] for y in range(h)] for x in range(w)]
			
			for i in range(len(cons)):
				secA = getsection(consec, cons[i][0], cons[i][1])
				secB = getsection(consec, cons[i][2], cons[i][3])
				self._gap_junction(net[cons[i][0]][cons[i][1]].section(secA), net[cons[i][2]][cons[i][3]].section(secB), contype["strength"])
				cons[i] = cons[i] + (secA, secB, contype["strength"])
			allconnections = allconnections + cons
		return allconnections
		
	# List all bidirectional connections with radius r and size w, h
	# Takes matrix dimensions as a tuple, bounds r <=, r > (which must be greater than 0)
	def _biconnections(self, (w, h), r, rg = 0.1):
		con = []
		tests = []
		topr = int(r) + 2
		for i in range(topr):
			for j in range(topr):				
				d = i*i + j*j
				if d <= r*r and d > rg*rg:
					tests.append((i,j))
		
		for x in range(w):
			for y in range(h):
				for t in tests:
 	  	 			if y + t[1] < h and x + t[0] < w:
						con.append((x, y, x+t[0], y+t[1]))
		return con
				
	# Create a bidirectional gap junction.
	# Takes section of cells (either cellA.list_dend[0] or cellA.section('a')), strength,
	# and optional positions in sections
	def _gap_junction(self, section_of_cell_A, section_of_cell_B, strength, section_pos_a = 0.5, section_pos_b = 0.5):
		# A -> B
		self.gap_junctions.append(nrn.Gap(section_pos_b, sec = section_of_cell_B))
		nrn.setpointer(section_of_cell_A(section_pos_a)._ref_v, 'vgap', self.gap_junctions[-1])
		self.gap_junctions[-1].ggap = strength
		# B -> A
		self.gap_junctions.append(nrn.Gap(section_pos_a, sec = section_of_cell_A))
		nrn.setpointer(section_of_cell_B(section_pos_b)._ref_v, 'vgap', self.gap_junctions[-1])
		self.gap_junctions[-1].ggap = strength	
		
if __name__ == '__main__':
	n = Network()
	n.cortical_network()