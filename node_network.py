import matplotlib as mpl 
mpl.use('Agg')
import matplotlib.pyplot as plt 
from supplemental.axes_create import fig_std

from math import ceil
import random as ran
import numpy as np
from sys import maxint

from class_FS import FS
from class_LTS import LTS
from class_RS import RS
from neuron import h as nrn
nrn.load_file("stdrun.hoc")

# This is a network file designed to run on a node (if run in parallel, which this code most
# certainly cannot do yet)
class Network():
	def __init__(self, seed=False):
		self.layout_seed = self.random_seed(seed)
			
		# All of our cells must be run at 30 degrees C. We will do this from the getgo
		nrn.celsius = 30
		self.gap_junctions = []
		self.stims = []
		self.records = []
		self.time = []
	
	# reseed random and return for storage
	def random_seed(self, seed=False):
		if seed is False:
			ran.seed()
			seed = ran.randint(0, maxint)
			ran.seed(seed)
		else:
			ran.seed(seed)
		return seed
		
	# Define a matrix of cells, x on outside
	def matrix(self, type, (x, y)):
		out = []
		for i in range(x):
			out.append([])
			for j in range(y):
				out[i].append(globals()[type]((x, y)))
		return out
		
	# Takes a network and list of dictionaries containing parameters
	# [{'probability', 'strength', 'radius', 'inner_radius'. 'sections':['a', 'b']}]
	def couple_network(self, net, pars):
		out = []
		for contype in pars:
			w = len(net)
			h = len(net[0])
			seclen = len(contype["sections"])
			connections = self.list_biconnections((w, h), contype["radius"], contype["inner_radius"])
			conned = [[0 for y in range(h)] for x in range(w)]
			
			out.append([])
			for con in connections:
				if ran.random() < contype["probability"]:
					self.gap_junction(net[con[0]][con[1]].section(contype["sections"][conned[con[0]][con[1]]%seclen]), net[con[2]][con[3]].section(contype["sections"][conned[con[2]][con[3]]%seclen]), contype["strength"])
					conned[con[0]][con[1]] += 1
					conned[con[2]][con[3]] += 1
					out.append((con[0], con[1], con[2], con[3], 1))
				else:
					out.append((con[0], con[1], con[2], con[3], 0))
		return out
		
	
	# Takes the matrix dimensions as a tuple, the radius less than or equal to, and a radius greater than (must be > 0)
	def list_biconnections(self, (w, h), r, rg = 0.1):
		con = []
		tests = []
		topr = int(ceil(r)) + 1
		for i in range(topr):
			for j in range(topr):				
				d = i*i + j*j
				if d <= r*r and d > rg*rg:
					tests.append((i,j))
		
		for x in range(w):
			for y in range(h):
				for t in tests:
 	  	 			if y + t[1] < h and x + t[0] < w:
						con.append((x, y, x+t[0], y+t[1]))
		return con
				
	
	# Takes as input the section of a cell. This could be cellA.list_dend[0] or cellA.section('a')
	# Defines a bidirectional gap junction. This could be done in class cell, but then it would
	# only take a unidirectional gap junction. We can implement that as an alternative, but this
	# is elegant.
	def gap_junction(self, section_of_cell_A, section_of_cell_B, strength, section_pos_a = 0.5, section_pos_b = 0.5):
		# Define the gap junction with B as receiver
		self.gap_junctions.append(nrn.Gap(section_pos_b, sec = section_of_cell_B))
		nrn.setpointer(section_of_cell_A(section_pos_a)._ref_v, 'vgap', self.gap_junctions[-1])
		self.gap_junctions[-1].ggap = strength
		
		# Define the gap junction with A as receiver
		self.gap_junctions.append(nrn.Gap(section_pos_a, sec = section_of_cell_A))
		nrn.setpointer(section_of_cell_B(section_pos_b)._ref_v, 'vgap', self.gap_junctions[-1])
		self.gap_junctions[-1].ggap = strength

	# Define a stimulus. You should include delay, dur, and amp
	def stimulus(self, segment, properties):
		stim = nrn.IClamp(segment)
		for key in properties:
			setattr(stim, key, properties[key])
		self.stims.append(stim)
			
	# Define a recording site
	def record(self, segment):
		if len(self.time) == 0:
			t = nrn.Vector()
			t.record(nrn._ref_t)
			self.time.append(t)
		
		v = nrn.Vector()
		v.record(segment._ref_v)
		self.records.append(v)
		
		
	def FS_network(self):
		self.fs = self.matrix('FS', (5, 5))
		pars = [{'probability':0.75, 'strength':3, 'radius':1, 'inner_radius':0.1, 'sections':['a', 'f', 'k']}, {'probability':0.6, 'strength':2, 'radius':2, 'inner_radius':1, 'sections':['c', 'h', 'm', 'd', 'i', 'n', 'e', 'j', 'o']},]
		self.couple_network(self.fs, pars)
		
		

def plot(name, t, varray):
	fig = fig_std()
	fig.ax0.hold(True)
	for i in range(len(varray)):
		fig.ax0.plot(t, varray[i])
	fig.ax0.set_ylim(-80, 50)
	plt.savefig(name + '.png')
	fig.close()

if __name__ == "__main__":
	net = Network()
	net.FS_network()
	
	net.stimulus(net.fs[0][0].soma(0.5), {'delay':50, 'dur':300, 'amp':0.5})
	net.record(net.fs[0][0].soma(0.5))
	net.record(net.fs[0][1].soma(0.5))
	
	nrn.tstop = 500
	nrn.run()
	
	plot('out', net.time[0], net.records)
	exit()