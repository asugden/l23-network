import matplotlib as mpl
mpl.use('Agg', warn=False)
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.gridspec as gridspec
from neuron import h as nrn
import random as ran
import math
import numpy as np
import os
from time import strftime, localtime

from class_Network import Network

class NetTest():
	def __init__(self, type='FS', size=(5,5)):
		nrn.load_file("stdrun.hoc")
		self.type = type
		self.size = size
		self.font()
		self.titling()
		
		# Initialize network type
		self.net = Network()
		if type == 'FS':
			self.net.FS_network(self.size)
			self.cells = self.net.FS
		else:
			self.net.LTS_network(self.size)
			self.cells = self.net.LTS
		
		self.pars()
		# Draw connections of the particular network

	# ====================================================================================
	# EXTERNAL FUNCTIONS
	
	# Calculate the average coupling coefficient for a set of cells of number n. Cells are
	# drawn from everywhere except for the borders
	def average_coupling(self, n, histogram_spacing=1):
		cxy = (int(self.size[0]/2), int(self.size[1]/2))
		test = [cxy]
		possible = []
		for i in range(1, self.size[0]-1):
			for j in range(1, self.size[1]-1):
				if i != cxy[0] or j != cxy[1]:
					possible.append((i, j))
		ran.shuffle(possible)
		test = test + possible[:n-1]
		
		ccs = []
		for cell in test:
			self.resistance(cell)
			ccs.append(self.coupling_coefficient(cell))
			self.voltage_trace(cell)
		self.distance_coupling(test, ccs, histogram_spacing)
		return self
	
	# Measure the coupling coefficients between all cells and the specified cell and plot
	# Takes the position of a cell as a tuple
	def coupling_coefficient(self, cell):
		print "Measuring coupling coefficient of cell " + str(cell)
		# Measure
		self.record()
		self.stimulus(cell)
		cc = self.coupling(cell)
		
		# Plot
		path = self.title + 'coupling-cell '+str(cell[0]+1)+','+str(cell[1]+1)
		title = 'COUPLING COEFFICIENT RELATIVE TO CELL '+str(cell[0]+1)+', '+str(cell[1]+1)
		self.heatmap(path, cc*100, 'purple', title, cell)
		
		return cc
	
	# Measures resistance in megaohms
	# Takes cell position, returns resistance unless path is set, then returns self
	def resistance(self, pos):
		print "Measuring resistance of cell " + str(pos)
		self.net.clearstimuli()
		self.net.clearrecords()
		self.net.record(self.cells[pos[0]][pos[1]].soma(0.5))
		self.net.stimulus(self.cells[pos[0]][pos[1]].soma(0.5), {'delay':100, 'dur':50, 'amp':-0.03})
		nrn.tstop = 150
		nrn.run()
		vc1 = self.net.records[0][int(99./nrn.dt)]
		vc2 = self.net.records[0][int(150/nrn.dt) - 2]
		r = (vc1 - vc2)*33.333
		fp = open(self.title + 'resistance-cell '+str(pos[0]+1)+','+str(pos[1]+1)+'.txt', 'w')
		fp.write("%.1f megaohms\n" % (r))
		fp.close()
		return self
	
	# Print voltage trace(s) to path with given bounds and size in inches
	# Cells can be a blank list, which will display all cells, a tuple, which will print
	# just that cell, or a list of tuples
	def voltage_trace(self, cells=[], bounds=[-120, -50], size=(8,6)):
		if isinstance(cells, tuple):
			cells = [cells]
		print "Drawing all voltage traces" if len(cells) == 0 else "Drawing voltage traces for cell(s) "+", ".join([str(i) for i in cells])
		name = "-".join([str(i) for i in cells]).replace('(', '')
			
		if len(cells) == 0:
			records = self.net.records
		else:
			records = []
			for cell in cells:
				records.append(self.net.records[cell[0]*self.size[1] + cell[1]])
		
		fig = plt.figure(figsize=(8, 6))
		ax = plt.subplot(111)
		
		ax.hold(True)
		for i in range(len(records)):
			ax.plot(self.net.time[0], records[i])
		ax.set_ylim(bounds[0], bounds[1])
		
		ax.set_title('VOLTAGE TRACES OF '+name if len(name) > 0 else 'ALL VOLTAGE TRACES')
		ax.set_xlabel('TIME (ms)')
		ax.set_ylabel('POTENTIAL (mV)')
		
		plt.savefig(self.title+'traces-cell '+name+'.png')		
		plt.close(fig)
		return self
	
	# Set the plotting font
	def font(self, font={'family':'Gotham', 'weight':'light', 'size':10}):
		mpl.rc('font', **font)
		return self	
		
	# ====================================================================================
	# INTERNAL FUNCTIONS
	
	# Plot a heatmap
	# Takes the save path, a numpy matrix of coefficients, and optional color and title
	def heatmap(self, path, npmatrix, color='red', title='', ref=-1, showlabels=True):
		fig = plt.figure()
		ax = plt.subplot(111)
		
		ax.set_xticks(np.arange(npmatrix.shape[1])+0.5, minor=False)
		ax.set_yticks(np.arange(npmatrix.shape[0])+0.5, minor=False)
		ax.set_xticklabels([i+1 for i in range(npmatrix.shape[1])], minor=False)
		ax.set_yticklabels([i+1 for i in range(npmatrix.shape[0])], minor=False)
		ax.set_title(title)
		
		colors = {'purple':plt.cm.PuRd, 'grey':plt.cm.Greys, 'red':plt.cm.Reds, 'blue':plt.cm.Blues}
		hm = plt.pcolor(npmatrix, cmap=colors[color])
		plt.colorbar()
		
		if ref != -1:
			fc = 'red' if color == 'grey' else 'black'
			ax.add_patch(mpl.patches.Rectangle(ref, 1, 1, facecolor=fc, edgecolor="white", hatch="/"))
			ax.text(ref[0]+0.5, ref[1]+0.5, "Reference", horizontalalignment='center', verticalalignment='center', color="white", fontsize=8)
		
		if showlabels:
			l = 'red' if color == 'grey' else 'black'
			for i in range(npmatrix.shape[1]):
				for j in range(npmatrix.shape[0]):
					if ref == -1 or (i != ref[0] or j != ref[1]):
						ax.text(i+0.5, j+0.5, "%.1f" % (npmatrix[j][i]), horizontalalignment='center', verticalalignment='center', color=l, fontsize=8)
		
		plt.savefig(path + '.pdf')		
		plt.close(fig)
		return self
	
	# Record from all cells. Use when changing a run. Automatically clears previous data
	def record(self):
		self.net.clearstimuli()
		self.net.clearrecords()
		for i in range(self.size[0]):
			for j in range(self.size[1]):
				self.net.record(self.cells[i][j].soma(0.5)) # Reference cells in terms of matrix[x][y]
		return self
	
	# Set a stimulus at a particular cell and run it
	# Takes position tuple (x,y)	
	def stimulus(self, pos):
		self.stimpars = {'delay':100, 'dur':300, 'amp':-0.3}
		self.stimpos = pos
		self.net.stimulus(self.cells[pos[0]][pos[1]].soma(0.5), self.stimpars)
		self.stimpars['baseline'] = int(99./nrn.dt)
		self.stimpars['equilibrium'] = int((self.stimpars['dur']+self.stimpars['delay'])/nrn.dt) - 2
		nrn.tstop = 400
		nrn.run()
		
	# Determine the appropriate path to save to and save parameters
	def titling(self, title=''):
		self.title = title if title != '' else strftime("%y%m%d-%H%M%S", localtime()) + '/'
		os.mkdir(self.title)
	
	# Save electrical coupling parameters
	# Takes a file pointer	
	def pars(self):
		file = open(self.title + 'pars.txt', 'w')
		file.write(self.type + ' Network %0dx%0d\nElectrical connections\n' % (self.size[0], self.size[1]))
		pars = self.net.FS_pars if self.type == 'FS' else self.net.LTS_pars
		
		for par in pars:
			file.write("    Prob:     %5.2f\n    Strength: %5.2f\n    Radius:    %.1f < r <= %.1f\n    Section:   " % (par['probability'], par['strength'], par['inner_radius'], par['radius']))
			file.write(", ".join(par['sections']))
			file.write('\n\n')	
		file.close()
		
	# Measure the electrical coupling between all cells and a stimulated cell
	# Takes the cell position as a tuple
	def coupling(self, pos):
		vc1 = self.net.records[pos[0]*self.size[1] + pos[1]][self.stimpars['baseline']]
		vc2 = self.net.records[pos[0]*self.size[1] + pos[1]][self.stimpars['equilibrium']]
		vdif = vc1 - vc2
		
		out = np.zeros(self.size[0]*self.size[1]).reshape(self.size[0], self.size[1])
		for i in range(self.size[0]):
			for j in range(self.size[1]):
				if i != pos[0] or j != pos[1]:
					vn1 = self.net.records[i*self.size[1] + j][self.stimpars['baseline']]
					vn2 = self.net.records[i*self.size[1] + j][self.stimpars['equilibrium']]
					out[i][j] = (vn1 - vn2)/vdif
		
		return out
		
	# Plot the average coupling coefficient by distance
	def distance_coupling(self, cells, coefficients, spacing=1):
		ds = []
		for i in range(len(cells)):
			for x in range(self.size[0]):
				for y in range(self.size[1]):
					d = math.sqrt((cells[i][0] - x)*(cells[i][0] - x) + (cells[i][1] - y)*(cells[i][1] - y))
					if d > 0:
						ds.append((d, coefficients[i][x][y]))
		
		dxy = (self.size[0] - 1, self.size[1] - 1)
		length = int(math.sqrt(dxy[0]*dxy[0] + dxy[1]*dxy[1])) + 1
		ns = np.zeros(length)
		avs = np.zeros(length)
		mins = np.zeros(length)
		maxes = np.zeros(length)
		
		for i in range(length):
			sub = [j[1] for j in ds if j[0] > i*spacing and j[0] <= (i+1)*spacing]
			if len(sub) > 0:
				ns[i] = len(sub)
				avs[i] = sum(sub)/float(len(sub))
				mins[i] = min(sub)
				maxes[i] = max(sub)
		
		
		fig = plt.figure(figsize=(6,6))
		ax = fig.add_subplot(111)
		ax.set_ylim(0, 20)
		ax.set_xlim(0, len(avs)*spacing)
		ax.set_title('AVERAGE COUPLING BY DISTANCE')
		ax.set_xlabel('DISTANCE')
		ax.set_ylabel('PERCENT COUPLING')
		
		ax.hold(True)
		ax.scatter(np.arange(len(maxes))*spacing + spacing/2., maxes*100, s=30, facecolors="#C73232", edgecolors="#666666", lw=0.5)
		ax.scatter(np.arange(len(mins))*spacing + spacing/2., mins*100, s=30, facecolors="#5732C7", edgecolors="#666666", lw=0.5)
		ax.bar(np.arange(len(avs))*spacing, avs*100, spacing, facecolor="#2B8CCC", edgecolor="#666666", alpha=0.75)
		
		plt.savefig(self.title+'average-coupling.pdf')		
		plt.close(fig)
			
	
	def localrecordings(self, pos):
		xmin = pos[0] - 2 if pos[0] - 2 >= 0 else 0
		xmax = pos[0] + 2 if pos[0] + 2 < self.size[0] else self.size[0] - 1
		ymin = pos[1] - 2 if pos[1] - 2 >= 0 else 0
		ymax = pos[1] + 2 if pos[1] + 2 < self.size[1] else self.size[1] - 1
		
		out = []
		if self.type.find('FS') > -1:
			for i in range(xmin, xmax+1, 1):
				for j in range(ymin, ymax+1, 1):
					self.net.record(self.net.FS[i][j].soma(0.5))
					out.append((i, j))
		if self.type.find('LTS') > -1:
			for i in range(xmin, xmax+1, 1):
				for j in range(ymin, ymax+1, 1):
					self.net.record(self.net.LTS[i][j].soma(0.5))
					out.append((i, j))
		return out
		
	def localcoupling(self, pos, others):
		print('Measuring coupling')
		for i in range(len(others)):
			if pos[0] == others[i][0] and pos[1] == others[i][1]:
				compare = i
		
		baseline = 3800
		understim = len(self.net.time[0]) - 200
		vc1 = self.net.records[compare][baseline]
		vc2 = self.net.records[compare][understim]
		vdif = vc1 - vc2
		
		out = []
		for i in range(len(self.net.records)):
			vn1 = self.net.records[i][baseline]
			vn2 = self.net.records[i][understim]
			out.append((vn1 - vn2)/vdif)
		return out
		
	def averagecoupling(self):
		print('Averaging...')
		counts = np.zeros(self.size[0]*self.size[1]).reshape(self.size[0], self.size[1])
		values = np.zeros(self.size[0]*self.size[1]).reshape(self.size[0], self.size[1])
		mins = np.zeros(self.size[0]*self.size[1]).reshape(self.size[0], self.size[1])
		maxes = np.zeros(self.size[0]*self.size[1]).reshape(self.size[0], self.size[1])
		
		
		for x in range(self.size[0]):
			for y in range(self.size[1]):
				print('Stimulus of cell '+str(x)+', '+str(y))
				pos = [x, y]
				others = self.clear().localrecordings(pos);
				self.setstim(pos)
				
				cc = self.localcoupling(pos, others)
				for i in range(len(cc)):
					if others[i][0] != pos[0] or others[i][1] != pos[1]:
						if counts[others[i][0]][others[i][1]] == 0:
							mins[others[i][0]][others[i][1]] = cc[i]
							maxes[others[i][0]][others[i][1]] = cc[i]
						elif mins[others[i][0]][others[i][1]] > cc[i]:
							mins[others[i][0]][others[i][1]] = cc[i]
						elif maxes[others[i][0]][others[i][1]] < cc[i]:
							maxes[others[i][0]][others[i][1]] = cc[i]
							
						counts[others[i][0]][others[i][1]] += 1
						values[others[i][0]][others[i][1]] += cc[i]
		
		coeffs = values/counts
		
		# Average
		fig = plt.figure()
		ax = plt.subplot(111)
		
		ax.set_xticks(np.arange(coeffs.shape[0])+0.5, minor=False)
		ax.set_yticks(np.arange(coeffs.shape[1])+0.5, minor=False)
		
		column_labels = [i+1 for i in range(coeffs.shape[0])]
		row_labels = [i+1 for i in range(coeffs.shape[1])]
		ax.set_xticklabels(row_labels, minor=False)
		ax.set_yticklabels(column_labels, minor=False)
		
		hm = plt.pcolor(coeffs*100, cmap=plt.cm.Greys)
		plt.colorbar()
		
		ax.set_title('AVERAGE COUPLING COEFFICIENTS')
		
		plt.savefig(self.title + 'coupling-average.pdf')		
		plt.close(fig)
		
		# Maxes
		fig = plt.figure()
		ax = plt.subplot(111)
		
		ax.set_xticks(np.arange(coeffs.shape[0])+0.5, minor=False)
		ax.set_yticks(np.arange(coeffs.shape[1])+0.5, minor=False)
		
		ax.set_xticklabels(row_labels, minor=False)
		ax.set_yticklabels(column_labels, minor=False)
		
		hm = plt.pcolor(maxes*100, cmap=plt.cm.Reds)
		plt.colorbar()
		
		ax.set_title('MAXIMUM COUPLING COEFFICIENTS')
		
		plt.savefig(self.title + 'coupling-max.pdf')		
		plt.close(fig)
		
		# Mins
		fig = plt.figure()
		ax = plt.subplot(111)
		
		ax.set_xticks(np.arange(coeffs.shape[0])+0.5, minor=False)
		ax.set_yticks(np.arange(coeffs.shape[1])+0.5, minor=False)
		
		ax.set_xticklabels(row_labels, minor=False)
		ax.set_yticklabels(column_labels, minor=False)
		
		hm = plt.pcolor(mins*100, cmap=plt.cm.Blues)
		plt.colorbar()
		
		ax.set_title('MINIMUM COUPLING COEFFICIENTS')
		
		plt.savefig(self.title + 'coupling-min.pdf')		
		plt.close(fig)
		

		

				
if __name__ == "__main__":
	n = NetTest('FS', (5, 5))
	n.net.draw_connections(n.title, (6, 6)) # tuple is size in inches
	n.average_coupling(2)
#	n.couplingheatmap().allplots().averagecoupling()