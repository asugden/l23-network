# FS.py - a class for an FS or PV cell, to be run at 30 Celsius
# Modeled after the FS cell of Vierling-Claassen 2010
# Retuned by G. Neske to fix eK, eNa, gbarNa, and gbarKv in 7/2012

from class_CellM import CellM
from neuron import h as nrn
	
class FS(CellM):
    def __init__(self, pos):
        self.parameters()
        
        # CellM.__init__(self, pos, L, diam, Ra, cm)
        CellM.__init__(self, pos, 'FS')

        self.create_dends(self.dendrite_section_properties, self.biophysics_all['cm']) # this is in CellM()
        self.connect_sections()
        self.biophys()
        self.synapse_create()
    
    # PARAMETERS ------------------------------------------------------------------------
    def parameters(self):
        # name, length, diameter
        self.dendrite_section_properties = [
            ('a', 3.4, 3),
            ('b', 8.4, 2),
            ('c', 143, 1.25),
            ('d', 117, 1.25),
            ('e', 140, 1.25),
            
            ('f', 3.4, 3),
            ('g', 8.4, 2),
            ('h', 143, 1.25),
            ('i', 117, 1.25),
            ('j', 140, 1.25),
            
            ('k', 3.4, 3),
            ('l', 8.4, 2),
            ('m', 143, 1.25),
            ('n', 117, 1.25),
            ('o', 140, 1.25),
        ]
        
        # connections by name, doesn't slow things down much
        # (connect me, at position, to her, at position)
        # subsets are now 0-4, 5-9, 10-14 describing each of the dendrites branching from the soma
        self.dendrite_connections = [
            ('a', 0, 'soma', 1),
            ('b', 0, 'a', 1),
            ('c', 0, 'b', 1),
            ('d', 0, 'b', 1),
            ('e', 0, 'a', 1),
            
            ('f', 0, 'soma', 1),
            ('g', 0, 'f', 1),
            ('h', 0, 'g', 1),
            ('i', 0, 'g', 1),
            ('j', 0, 'f', 1),
            
            ('k', 0, 'soma', 1),
            ('l', 0, 'k', 1),
            ('m', 0, 'l', 1),
            ('n', 0, 'l', 1),
            ('o', 0, 'k', 1),
        ]
        
        
        # list of receptors to add to all dendritic sections
        self.receptors = ['ampa', 'nmda', 'gabaa', 'gabab', 'ampaf', 'ampad']

        # list channels to insert everywhere
        self.channels = ['pas', 'kv', 'na_golomb', 'ca', 'kca', 'km', 'kv3']
        
        # biophysics adjustments to be applied to all compartments, soma, and dendrites
        self.biophysics_all = {
            'g_pas':1.2e-4,
            'e_pas':-73,
            'Ra':200,
            'cm':1,
            'ek':-65,
#            'ena':60,
            'eca':140,
            'gbar_na_golomb':0.07, # was 0.1125
        }
        
        self.biophysics_soma = {
            'L':8.2,
            'diam':13.2,
#            'gbar_na':2600, # was 1400, increasing sodium reduces first peak/other peak ratio, tightens width
            'gbar_kv':1500, # was 2700, was 500 with golomb na, slows down spiking
            'gbar_ca':0,
            'gbar_kca':0,
            'gbar_km':0,
            'gbar_kv3':10, # was 15
            # na:2000, kv3:8
        }
        
        self.biophysics_dendrites = {
#            'gbar_na':450, # was 350
            'gbar_kv':400, # was 350, was 150 with golomb na
            'gbar_ca':0,
            'gbar_kca':0,
            'gbar_km':0,
            'gbar_kv3':3, # was 5
        }
