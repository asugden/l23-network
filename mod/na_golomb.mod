COMMENT
Sodium channel, HH style kinetics
Using equation and constants from Golomb et al. Plos Comp Biol 2007
Coded by Arthur Sugden 2013
ENDCOMMENT

INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	SUFFIX na_golomb
	USEION na READ ena WRITE ina
	RANGE m, h, gna, gbar, ina, vshift
	RANGE hinf, htau
}

PARAMETER {
	gbar   = 112.5 (mS/cm2)
	v_na   = 50    (mV)
	
	vshift = 0	   (mV)	: voltage shift (affects all)
	
	: h
	sig_h  = -6.7  (mV)
	th_h   = -58.3 (mV)
	sig_th = -12   (mV)
	th_th  = -60   (mV)
	
	: m
	sig_m  = 11.5  (mV)
	th_m   = -26   (mV) : -24 to -28 to vary firing properties, Golomb

	v 			   (mV)
	dt			   (ms)
	celsius		   (degC)
	vmin   = -120  (mV)
	vmax   = 100   (mV)
}


UNITS {
	(uA) = (microamps)
	(mV) = (millivolts)
	(mS) = (millisiemens)
} 

ASSIGNED {
	ina  (uA/cm2)
	gna	 (mS/cm2)
	ena	 (mV)
	htau (ms)
	hinf
} 

STATE { m h }

INITIAL { 
	rates(v + vshift)
	h = 0
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = gbar*m*m*m*h
	ina = gna*(v - v_na)
}

DERIVATIVE states {
    rates(v + vshift)
    h' =  (hinf - h)/htau
}

PROCEDURE rates(vm) {  
	: "m" activation
	m = 1/(1 + exp(-(vm - th_m)/sig_m))

	: "h" inactivation 
	hinf = 1/(1 + exp(-(vm - th_h)/sig_h))
	htau = 0.5 + 14/(1 + exp(-(vm - th_th)/sig_th))
}
