NEURON {
  POINT_PROCESS noise
  RANGE stdev
  NONSPECIFIC_CURRENT i
}

UNITS {
  (nA) = (nanoamp)
  (uA) = (microamp)
}

PARAMETER {
	stdev = 1
	amp = 2
	del = 1    (ms)
	dur = 100   (ms)
}

ASSIGNED {
  i (nA)
  ival (nA)
  on (1)
}

INITIAL {
  i = 0
  on = 0
  net_send(del, 1)
}

PROCEDURE seed(x) {
  set_seed(x)
}

BEFORE BREAKPOINT {
	LOCAL noise
  if (on) {
  	noise = amp*normrand(0, stdev)
    ival = sqrt(noise)
  } else {
    ival = 0
  }
}

BREAKPOINT {
  i = ival
}

NET_RECEIVE (w) {
  if (flag == 1) {
    if (on == 0) {
      : turn it on
      on = 1
      : prepare to turn it off
      net_send(dur, 1)
    } else {
      : turn it off
      on = 0
    }
  }
}