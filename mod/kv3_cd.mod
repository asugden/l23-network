TITLE Fast Delayed Rectifier K Channel
: from Gunay, Egerton and Jaeger (2008)
: Ported to NEURON .mod extensions by Chris Deister 11/2009
: I don't know what to make of the hyperpolarized V1/5 for the activation gate. I will have to ask Jeremy.
: Changed gkbar to gbar, AUS 4/19/2013

NEURON {
	SUFFIX kv3
	USEION k READ ek WRITE ik
    RANGE gbar, gk, vmInf, vhInf, km, kh, minf, hinf
    RANGE taum, tauh, q10
}

UNITS {
	(mA) = (milliamp)
	(mV) = (millivolt)

}

PARAMETER {
	v 			(mV)
	celsius		(degC)
	gbar=.02   (mho/cm2)
    vhalfm=-26  (mV)
    vhalfh=-20  (mV)
	km=7.8		(mV)
	kh=-10		(mV)
	q10=3
	ek			(mV)
	
	taumMin=0.1		(ms)
	taumMax=14 		(ms)
	tauhMin=7 		(ms)
	tauhMax=33		(ms)
	
	vhalfTaum=-26 	(mV)
	ktaum1=13 		(mV)
	ktaum2=-12 		(mV)
	
	vhalfTauh=0 	(mV)
	ktauh1=10 		(mV)
	ktauh2=-10 		(mV)
}

STATE {
	m
    h
}

ASSIGNED {
	ik (mA/cm2)
	minf
    hinf
    taum
	tauh
    gk
}

INITIAL {
	rates(v)
	m=minf
	h=hinf
}

BREAKPOINT {
	SOLVE states METHOD cnexp
	gk = gbar*m^4*h
	ik = gk*(v-ek)

}

DERIVATIVE states {
        rates(v)
        m' = (minf - m)/taum
		h' = (hinf - h)/tauh
}

PROCEDURE rates(v (mV)) { :callable from hoc
        LOCAL qt
       
 		qt=q10^((celsius-24)/10)
	
		minf=1/(1+exp((vhalfm-v)/km))
		hinf=0.6+((1-0.6)/(1+exp((vhalfh-v)/kh)))      
		
		taum = taumMin+((taumMax-taumMin)/((exp((vhalfTaum-v)/ktaum1))+(exp((vhalfTaum-v)/ktaum2))))
		tauh = tauhMin+((tauhMax-tauhMin)/((exp((vhalfTauh-v)/ktauh1))+(exp((vhalfTauh-v)/ktauh2))))
}