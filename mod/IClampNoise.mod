NEURON {
  POINT_PROCESS IClampNoise
  RANGE i,del,dur,f0,f1,r,torn,std
  ELECTRODE_CURRENT i
}

UNITS {
  (nA) = (nanoamp)
}

PARAMETER {
  del=0    (ms)
  dur=1000   (ms)
  torn=500  (ms)
  std=0.05   (nA)
  f0=0.2    (nA)
  f1=0.8    (nA)
  r =60
}

ASSIGNED {
  ival (nA)
  i (nA)
  amp (nA)
  on (1)
}

INITIAL {
  i = 0
  on = 0
:  net_send(del, 1)
}

PROCEDURE seed(x) {
  set_seed(x)
}

BEFORE BREAKPOINT {
:  if  (on) {
    ival = normrand(0, std*1(/nA))*1(nA)
 : } else {
:    ival = 0
:  }
}

BREAKPOINT {
  i = ival
}

NET_RECEIVE (w) {
  if (flag == 1) {
    if (on == 0) {
      : turn it on
      on = 1
      : prepare to turn it off
      net_send(dur, 1)
    } else {
      : turn it off
      on = 0
    }
  }
}